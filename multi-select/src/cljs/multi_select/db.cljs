(ns multi-select.db)

(def default-db
  {:name "re-frame"
   ;; Original items
   ;;   :items [{:id 1 :desc "Apple"}
   ;;           {:id 2 :desc "Orange"}
   ;;           {:id 3 :desc "Banana"}]
   :items {1 {:id 1 :desc "Apple"}
           2 {:id 2 :desc "Orange"}
           3 {:id 3 :desc "Banana"}}
   })
