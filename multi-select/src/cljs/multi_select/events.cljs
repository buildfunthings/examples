(ns multi-select.events
    (:require [re-frame.core :as re-frame]
              [multi-select.db :as db]))

(re-frame/reg-event-db
 :initialize-db
 (fn  [_ _]
   db/default-db))

(re-frame/reg-event-db
 :select-item
 (fn [db [_ item-id]]
   (update-in db [:items item-id :selected?] not)))

;; (re-frame/reg-event-db
;;  :select-item
;;  (fn [db [_ item-id]]
;;    (let [items (:items db)]
;;      (assoc db :items
;;             (map #(if (= item-id (:id %))
;;                     (assoc % :selected? (not (:selected? %)))
;;                     %)
;;                  items)))))
