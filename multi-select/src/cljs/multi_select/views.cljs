(ns multi-select.views
    (:require [re-frame.core :as re-frame]))

;; Original
;; (defn- create-checklist-item [{:keys [id desc selected?]}]
;;   ^{:key id}
;;   [:li.list-group-item {:on-click  #(re-frame/dispatch [:select-item id])
;;                         :class (when selected? "active")
;;                         :data-checked selected?}
;;    desc])

(defn- create-checklist-item [[_ {:keys [id desc selected?]}]]
  ^{:key id}
  [:li.list-group-item {:on-click  #(re-frame/dispatch [:select-item id])
                        :class (when selected? "active")
                        :data-checked selected?}
   desc])

(defn main-panel []
  (let [items (re-frame/subscribe [:items])]
    (fn []
      [:div
       [:div.row
        [:div.col-xs-12
         [:h3.text-center "Select item"]
         [:div.well
          [:ul.list-group.checked-list-box
           (map create-checklist-item @items)
           ]]]]
       
       [:div.row
         [:pre.alert.alert-success (with-out-str (cljs.pprint/pprint @items))]]]
       ))
  )
