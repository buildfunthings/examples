(ns project-settings.core
  (:require [reagent.core :as reagent]))

(goog-define project-version "unknown")

(enable-console-print!)

(defn show-settings []
  [:div "Version: " [:b project-version]])

(defn mount-root []
  (reagent/render [show-settings]
                  (.getElementById js/document "app")))

(defn ^:export init []
  (mount-root)
  )
